<?php
/**
 * Arquivo de definição de diretórios da aplicação
 * 
 * @author Raphael Almeida Araújo <raphox.araujo@gmail.com>
 * @package Smarty com MVC
 * @version 0.6
 * @license GNU Version 2, June 1991
 */

define('DIR_CONFIG',  DIR_ROOT .   DIRECTORY_SEPARATOR . 'config');
define('DIR_APPS',    DIR_ROOT .   DIRECTORY_SEPARATOR . 'apps');
define('DIR_CACHE',   DIR_ROOT .   DIRECTORY_SEPARATOR . 'cache');
define('DIR_LIBS',    DIR_ROOT .   DIRECTORY_SEPARATOR . 'libs');
define('DIR_MODELS',  DIR_LIBS .   DIRECTORY_SEPARATOR . 'models');
define('DIR_PUBLIC',  DIR_ROOT .   DIRECTORY_SEPARATOR . 'public');
define('DIR_UPLOAD',  DIR_PUBLIC . DIRECTORY_SEPARATOR . 'upload');

define('DIR_CONTROLLERS', DIR_APPS . DIRECTORY_SEPARATOR . APP . DIRECTORY_SEPARATOR . 'controllers');

/**
 * Dados de configuração do Doctrine
 */
define('DOCTRINE_PATH',      DIR_ROOT . DIRECTORY_SEPARATOR . 'libs');
define('DATA_FIXTURES_PATH', DIR_ROOT . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'fixtures');
define('MODELS_PATH',        DIR_ROOT . DIRECTORY_SEPARATOR . 'libs' . DIRECTORY_SEPARATOR . 'models');
define('MIGRATIONS_PATH',    DIR_ROOT . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'migrations');
define('SQL_PATH',           DIR_ROOT . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'sql');
define('YAML_SCHEMA_PATH',   DIR_ROOT . DIRECTORY_SEPARATOR . 'config');
?>
