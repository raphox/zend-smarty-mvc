<?php
/**
 * Arquivo de inicialização do sistema
 *
 * @author Raphael Almeida Araújo <raphox.araujo@gmail.com>
 * @package Smarty com MVC
 * @version 0.6
 * @license GNU Version 2, June 1991
 */

session_start();

require_once(DIR_CONFIG    . DIRECTORY_SEPARATOR . 'config.php');
require_once(DIR_LIBS      . DIRECTORY_SEPARATOR . 'Doctrine.php');
require_once(DIR_LIBS      . DIRECTORY_SEPARATOR . 'Smarty' . DIRECTORY_SEPARATOR . 'Smarty.class.php');
require_once(DIR_MODELS    . DIRECTORY_SEPARATOR . 'View.php');
require_once(DIR_MODELS    . DIRECTORY_SEPARATOR . 'Controller.php');
require_once(DOCTRINE_PATH . DIRECTORY_SEPARATOR . 'Doctrine.php');

/**
 * Inicialização do Doctrine
 */
spl_autoload_register(array('Doctrine', 'autoload'));

Doctrine_Manager::connection('mysql://' . $GLOBALS['DATABASE']['user'] .
	':' . $GLOBALS['DATABASE']['password'] .
	'@' . $GLOBALS['DATABASE']['host'] .
	'/' . $GLOBALS['DATABASE']['database']);

Doctrine_Manager::getInstance()->setAttribute('model_loading', 'conservative');
Doctrine::loadModels(DIR_MODELS);

$conn = Doctrine_Manager::connection();
$conn->setListener(new MyListener());

?>