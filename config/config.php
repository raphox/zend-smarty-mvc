<?php

/**
 * Arquivo contendo configuracao de acesso a banco
 * e outras constantes do sistema
 *
 * @author Raphael Almeida Araújo <raphox.araujo@gmail.com>
 * @package Smarty com MVC
 * @version 0.6
 * @license GNU Version 2, June 1991
 */

$GLOBALS['DATABASE'] = array(
    'host'     => 'localhost',
    'user'     => 'root',
    'password' => 'alo123',
    'database' => 'freedomday'
  )
;

$GLOBALS['PHPDEBUG'] = array(
    'HTML_DIV_images_path' => '/images', 
    'HTML_DIV_css_path' => '/css', 
    'HTML_DIV_js_path' => '/js',
  )
;
?>