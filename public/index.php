<?php
  define('DIR_ROOT',    realpath(dirname(__FILE__).'/..'));
  define('APP',         'frontend');
  
  require_once(DIR_ROOT   . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "dir.conf.php");
  require_once(DIR_CONFIG . DIRECTORY_SEPARATOR . "init.php");
  
  include_once('PHP/Debug.php');
  
  // Debug object
  $GLOBALS['DEBUGBAR'] = new PHP_Debug($GLOBALS['PHPDEBUG']);
  
  View::display();
  
  $GLOBALS['DEBUGBAR']->display();
?>