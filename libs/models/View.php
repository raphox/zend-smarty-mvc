��������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������set(self::$vars['controller']))
      {
        $array = array();
        foreach (explode('_', self::$vars['controller']) as $str)
        {
          $array[] = strtolower($str);
        }
        self::$vars['controller'] = implode('', $array);
      }

      if (isset(self::$vars['action']))
      {
        $array = array();
        foreach (explode('_', self::$vars['action']) as $str)
        {
          $array[] = ucfirst(strtolower($str));
        }
        self::$vars['action'] = implode('', $array);
        self::$vars['action'][0] = strtolower(self::$vars['action'][0]);
      }
    }
    return (isset(self::$vars[$var_name])) ? self::$vars[$var_name] : null;
  }

  /**
   * Dado o controller e action é feito o carremento e instanciamento
   * dos seus respectivos objetos e é impresso o resultado
   *
   */
  static public function display()
  {
    // define um modulo padrao
    $controller = 'Index';
    $action     = 'index';

    try
    {
      if (self::getRequestVar('controller')) {
        $controller = self::getRequestVar('controller');
      }

      if (self::getRequestVar('action')) {
        $action = self::getRequestVar('action');
      }

      if (file_exists(DIR_CONTROLLERS . DIRECTORY_SEPARATOR . ucfirst($controller) . 'Controller.php'))
      {
        require_once(DIR_CONTROLLERS . DIRECTORY_SEPARATOR . ucfirst($controller) . 'Controller.php');
      }
      else
      {
        throw new Exception('Controlador não encontrado!');
      }

      if (class_exists($controller . 'Controller'))
      {
        eval('$instancia = new ' . $controller . 'Controller("'.APP.'");');

        if (method_exists($instancia, $action.'Execute'))
        {
          eval('$instancia->' . $action."Execute();");
        }
        else
        {
          throw new Exception('Ação não encontrada!');
        }
      }
      else
      {
        throw new Exception('Classe não encontrada!');
      }
    }
    catch (Exception $e)
    {
      die($e);
      header("Location: /500.html");
    }
    
    // Limpando sessao flash
    unset($_SESSION['flash']);
  }
}
?>