<?php

/**
 * Classe responsavel por inicializa o objeto Smarty
 * e é reponsavel por gerenciar as variaveis passadas
 * ao Smarty e mostrar o ressultado da renderização
 *
 * @author Raphael Almeida Araújo <raphox.araujo@gmail.com>
 * @package Smarty com MVC
 * @version 0.6
 * @license GNU Version 2, June 1991
 */

/**
 * @name Controller
 * @var Smarty $smarty
 * @var Array unknown $flash
 * @var Array String $url_vars
 * @method __construct
 * @method getTypeRequest
 * @method redirect($url)
 * @method saveFlash
 * @method loadFlash
 * @method assingVars
 * @method renderrender($options = array())
 *
 */
class Controller
{
  protected $smarty;
  protected $flash;
  private $url_vars;

  protected function __construct($appName)
  {
    $this->smarty = new Smarty();

    $this->smarty->template_dir = DIR_APPS . DIRECTORY_SEPARATOR . $appName . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR;
    $this->smarty->compile_dir  = DIR_APPS . DIRECTORY_SEPARATOR . $appName . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'tpl_compiled' . DIRECTORY_SEPARATOR;
    $this->smarty->config_dir   = DIR_ROOT . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR;
    $this->smarty->cache_dir    = DIR_ROOT . DIRECTORY_SEPARATOR . 'cache' .DIRECTORY_SEPARATOR;
    
    $this->url_vars = View::getVars();
  }

  protected function getTypeRequest()
  {
    if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
    {
      return 'js';
    }
    else
    {
      return 'html';
    }
  }

  protected function redirect($url)
  {
    $this->saveFlash();
    header('Location: ' . $url);
    die();
  }
  
  protected function saveFlash()
  {
    $_SESSION['flash'] = $this->flash;
  }
  
  protected function loadFlash()
  {
    if (isset($_SESSION['flash']))
    {
      $this->smarty->assign('flash_vars', $_SESSION['flash']);
    }
  }
  
  protected function assignVars()
  {
    $this->loadFlash();

    $vars = get_object_vars($this);
    foreach ($vars as $key => $var)
    {
      $this->smarty->assign($key, $var);
    }
  }
  
  protected function render($options = array())
  {
    if (isset($options) && !is_array($options))
    {
      throw new InvalidArgumentException();
    }
    
    $uri = array();
    
    if (isset($options['controller']) && !empty($options['controller']))
    {
      $uri[] = $options['controller'];
    }
    else if (isset($this->url_vars['controller']) && !empty($this->url_vars['controller']))
    {
      $uri[] = $this->url_vars['controller'];
    }
    else
    {
      $uri[] = 'index';
    }
    
    if (isset($options['action']) && !empty($options['action']))
    {
      $uri[] = $options['action'];
    }
    else if (isset($this->url_vars['action']) && !empty($this->url_vars['action']))
    {
      $uri[] = $this->url_vars['action'];
    }
    else
    {
      $uri[] = 'index';
    }
    
    $this->assignVars();

    return $this->smarty->display(implode(DIRECTORY_SEPARATOR, $uri) . '.tpl');
  }
}

?>