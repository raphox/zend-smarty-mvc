                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     *
     * @return Doctrine_Table               the associated table object
     */
    public function getTable()
    {
        return $this->_table;
    }

    /**
     * sets the last used invoker
     *
     * @param Doctrine_Record $invoker      the record that invoked the last delegated call
     * @return Doctrine_Template            this object
     */
    public function setInvoker(Doctrine_Record $invoker)
    {
        $this->_invoker = $invoker;
    }

    /**
     * returns the last used invoker
     *
     * @return Doctrine_Record              the record that invoked the last delegated call
     */
    public function getInvoker()
    {
        return $this->_invoker;
    }

    /**
     * Adds a plugin as a child to this plugin
     * 
     * @param Doctrine_Template $template 
     * @return Doctrine_Template. Chainable.
     */
    public function addChild(Doctrine_Template $template)
    {
        $this->_plugin->addChild($template);
        
        return $this;
    }

    /**
     * Get plugin instance 
     * 
     * @return void
     */
    public function getPlugin()
    {
        return $this->_plugin;
    }

    /**
     * get 
     * 
     * @param mixed $name 
     * @return void
     */
    public function get($name) 
    {
        throw new Doctrine_Exception("Templates doesn't support accessors.");
    }

    /**
     * set 
     * 
     * @param mixed $name 
     * @param mixed $value 
     * @return void
     */
    public function set($name, $value)
    {
        throw new Doctrine_Exception("Templates doesn't support accessors.");
    }

    /**
     * Blank method for template setup 
     * 
     * @return void
     */
    public function setUp()
    {

    }

    /**
     * Blank method for template table definition
     * 
     * @return void
     */
    public function setTableDefinition()
    {

    }
}