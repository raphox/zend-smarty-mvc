                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ar string $_dql The view dql string
     */
    protected $_dql;

    /**
     * @var string $_sql The view sql string
     */
    protected $_sql;

    /**
     * constructor
     *
     * @param Doctrine_Query $query
     */
    public function __construct(Doctrine_Query $query, $viewName)
    {
        $this->_name  = $viewName;
        $this->_query = $query;
        $this->_query->setView($this);
        $this->_conn   = $query->getConnection();
        $this->_dql = $query->getDql();
        $this->_sql = $query->getSql();
    }

    /**
     * returns the associated query object
     *
     * @return Doctrine_Query
     */
    public function getQuery()
    {
        return $this->_query;
    }

    /**
     * returns the name of this view
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * returns the connection object
     *
     * @return Doctrine_Connection
     */
    public function getConnection()
    {
        return $this->_conn;
    }

    /**
     * creates this view
     *
     * @throws Doctrine_View_Exception
     * @return void
     */
    public function create()
    {
        $sql = sprintf(self::CREATE, $this->_name, $this->_query->getQuery());
        try {
            $this->_conn->execute($sql);
        } catch(Doctrine_Exception $e) {
            throw new Doctrine_View_Exception($e->__toString());
        }
    }

    /**
     * drops this view from the database
     *
     * @throws Doctrine_View_Exception
     * @return void
     */
    public function drop()
    {
        try {
            $this->_conn->execute(sprintf(self::DROP, $this->_name));
        } catch(Doctrine_Exception $e) {
            throw new Doctrine_View_Exception($e->__toString());
        }
    }

    /**
     * returns a collection of Doctrine_Record objects
     *
     * @return Doctrine_Collection
     */
    public function execute()
    {
        return $this->_query->execute();
    }

    /**
     * returns the select sql for this view
     *
     * @return string
     */
    public function getSelectSql()
    {
        return sprintf(self::SELECT, $this->_name);
    }

    /**
     * Get the view sql string
     *
     * @return string $sql
     */
    public function getViewSql()
    {
        return $this->_sql;
    }

    /**
     * Get the view dql string
     *
     * @return string $dql
     */
    public function getViewDql()
    {
        return $this->_dql;
    }
}