                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                t RECORD_SAVE        = 22;
    const RECORD_UPDATE      = 23;
    const RECORD_INSERT      = 24;
    const RECORD_SERIALIZE   = 25;
    const RECORD_UNSERIALIZE = 26;
    const RECORD_DQL_DELETE  = 27;
    const RECORD_DQL_SELECT  = 28;
    const RECORD_DQL_UPDATE  = 29;
    const RECORD_VALIDATE    = 30;

    /**
     * @var mixed $_invoker             the handler which invoked this event
     */
    protected $_invoker;

    /**
     * @var string $_query              the sql query associated with this event (if any)
     */
    protected $_query;

    /**
     * @var string $_params             the parameters associated with the query (if any)
     */
    protected $_params;

    /**
     * @see Doctrine_Event constants
     * @var integer $_code              the event code
     */
    protected $_code;

    /**
     * @var integer $_startedMicrotime  the time point in which this event was started
     */
    protected $_startedMicrotime;

    /**
     * @var integer $_endedMicrotime    the time point in which this event was ended
     */
    protected $_endedMicrotime;

    /**
     * @var array $_options             an array of options
     */
    protected $_options = array();

    /**
     * constructor
     *
     * @param Doctrine_Connection|Doctrine_Connection_Statement|
              Doctrine_Connection_UnitOfWork|Doctrine_Transaction $invoker   the handler which invoked this event
     * @param integer $code                                                  the event code
     * @param string $query                                                  the sql query associated with this event (if any)
     */
    public function __construct($invoker, $code, $query = null, $params = array())
    {
        $this->_invoker = $invoker;
        $this->_code    = $code;
        $this->_query   = $query;
        $this->_params  = $params;
    }

    /**
     * getQuery
     *
     * @return string       returns the query associated with this event (if any)
     */
    public function getQuery()
    {
        return $this->_query;
    }

    /**
     * getName
     * returns the name of this event
     *
     * @return string       the name of this event
     */
    public function getName()
    {
        switch ($this->_code) {
            case self::CONN_QUERY:
                return 'query';
            case self::CONN_EXEC:
                return 'exec';
            case self::CONN_PREPARE:
                return 'prepare';
            case self::CONN_CONNECT:
                return 'connect';
            case self::CONN_CLOSE:
                return 'close';
            case self::CONN_ERROR:
                return 'error';

            case self::STMT_EXECUTE:
                return 'execute';
            case self::STMT_FETCH:
                return 'fetch';
            case self::STMT_FETCHALL:
                return 'fetch all';

            case self::TX_BEGIN:
                return 'begin';
            case self::TX_COMMIT:
                return 'commit';
            case self::TX_ROLLBACK:
                return 'rollback';

            case self::SAVEPOINT_CREATE:
                return 'create savepoint';
            case self::SAVEPOINT_ROLLBACK:
                return 'rollback savepoint';
            case self::SAVEPOINT_COMMIT:
                return 'commit savepoint';

            case self::RECORD_DELETE:
                return 'delete record';
            case self::RECORD_SAVE:
                return 'save record';
            case self::RECORD_UPDATE:
                return 'update record';
            case self::RECORD_INSERT:
                return 'insert record';
            case self::RECORD_SERIALIZE:
                return 'serialize record';
            case self::RECORD_UNSERIALIZE:
                return 'unserialize record';
            case self::RECORD_DQL_SELECT:
                return 'select records';
            case self::RECORD_DQL_DELETE:
                return 'delete records';
            case self::RECORD_DQL_UPDATE:
                return 'update records';
            case self::RECORD_VALIDATE:
                return 'validate record';
        }
    }

    /**
     * getCode
     *
     * @return integer      returns the code associated with this event
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * getOption
     * returns the value of an option
     *
     * @param string $option    the name of the option
     * @return mixed
     */
    public function __get($option)
    {
        if ( ! isset($this->_options[$option])) {
            return null;
        }

        return $this->_options[$option];
    }

    /**
     * skipOperation
     * skips the next operation
     * an alias for __set('skipOperation', true)
     *
     * @return Doctrine_Event   this object
     */
    public function skipOperation()
    {
        $this->_options['skipOperation'] = true;

        return $this;
    }

    /**
     * setOption
     * sets the value of an option
     *
     * @param string $option    the name of the option
     * @param mixed $value      the value of the given option
     * @return Doctrine_Event   this object
     */
    public function __set($option, $value)
    {
        $this->_options[$option] = $value;

        return $this;
    }

    /**
     * setOption
     * sets the value of an option by reference
     *
     * @param string $option    the name of the option
     * @param mixed $value      the value of the given option
     * @return Doctrine_Event   this object
     */
    public function set($option, &$value)
    {
        $this->_options[$option] =& $value;

        return $this;
    }

    /**
     * start
     * starts the internal timer of this event
     *
     * @return Doctrine_Event   this object
     */
    public function start()
    {
        $this->_startedMicrotime = microtime(true);
    }������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������