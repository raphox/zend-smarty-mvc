��������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������ublic function __construct(array $defaults = null)
    {
        if (null !== $defaults) {
            foreach ($defaults as $name => $resource) {
                if ($resource instanceof Doctrine_Locator_Injectable) {
                    $resource->setLocator($this);
                }
                $this->_resources[$name] = $resource;
            }
        }
        self::$_instances[] = $this;
    }

    /** 
     * instance
     *
     * @return Sensei_Locator
     */
    public static function instance()
    {
        if (empty(self::$_instances)) {
            $obj = new Doctrine_Locator();
        }
        return current(self::$_instances);
    }

    /**
     * setClassPrefix
     *
     * @param string $prefix
     */
    public function setClassPrefix($prefix) 
    {
        $this->_classPrefix = $prefix;
    }

    /**
     * getClassPrefix
     *
     * @return string
     */
    public function getClassPrefix()
    {
        return $this->_classPrefix;
    }

    /**
     * contains
     * checks if a resource exists under the given name
     *
     * @return boolean      whether or not given resource name exists
     */
    public function contains($name)
    {
        return isset($this->_resources[$name]);
    }

    /**
     * bind
     * binds a resource to a name
     *
     * @param string $name      the name of the resource to bind
     * @param mixed $value      the value of the resource
     * @return Sensei_Locator   this object
     */
    public function bind($name, $value)
    {
        $this->_resources[$name] = $value;
        
        return $this;
    }

    /**
     * locate
     * locates a resource by given name and returns it
     *
     * @throws Doctrine_Locator_Exception     if the resource could not be found
     * @param string $name      the name of the resource
     * @return mixed            the located resource
     */
    public function locate($name)
    {
        if (isset($this->_resources[$name])) {
            return $this->_resources[$name];
        } else {
            $className = $name;

            if ( ! class_exists($className)) {

                $name = explode('.', $name);
                $name = array_map('strtolower', $name);
                $name = array_map('ucfirst', $name);
                $name = implode('_', $name);
                
                $className = $this->_classPrefix . $name;
                
                if ( ! class_exists($className)) {
                    throw new Doctrine_Locator_Exception("Couldn't locate resource " . $className);
                }
            }

            $this->_resources[$name] = new $className();

            if ($this->_resources[$name] instanceof Doctrine_Locator_Injectable) {
                $this->_resources[$name]->setLocator($this);
            }

            return $this->_resources[$name];
        }

        throw new Doctrine_Locator_Exception("Couldn't locate resource " . $name);
    }

    /**
     * count
     * returns the number of bound resources associated with
     * this object
     *
     * @see Countable interface
     * @return integer              the number of resources
     */
    public function count()
    {
        return count($this->_resources);
    }

    /**
     * getIterator
     * returns an ArrayIterator that iterates through all 
     * bound resources
     *
     * @return ArrayIterator    an iterator for iterating through 
     *                          all bound resources
     */
    public function getIterator()
    {
        return new ArrayIterator($this->_resources);
    }
}
