                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                replace('_', '-', Doctrine_Inflector::tableize(str_replace('Doctrine_Task_', '', get_class($this))));
    }

    /**
     * notify
     *
     * @param string $notification 
     * @return void
     */
    public function notify($notification = null)
    {
        if (is_object($this->dispatcher) && method_exists($this->dispatcher, 'notify')) {
            $args = func_get_args();
            
            return call_user_func_array(array($this->dispatcher, 'notify'), $args);
        } else if ( $notification !== null ) {
            return $notification;
        } else {
            return false;
        }
    }

    /**
     * ask
     *
     * @return void
     */
    public function ask()
    {
        $args = func_get_args();
        
        call_user_func_array(array($this, 'notify'), $args);
        
        $answer = strtolower(trim(fgets(STDIN)));
        
        return $answer;
    }

    /**
     * execute
     *
     * Override with each task class
     *
     * @return void
     * @abstract
     */
    abstract function execute();

    /**
     * validate
     *
     * Validates that all required fields are present
     *
     * @return bool true
     */
    public function validate()
    {
        $requiredArguments = $this->getRequiredArguments();
        
        foreach ($requiredArguments as $arg) {
            if ( ! isset($this->arguments[$arg])) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * addArgument
     *
     * @param string $name 
     * @param string $value 
     * @return void
     */
    public function addArgument($name, $value)
    {
        $this->arguments[$name] = $value;
    }

    /**
     * getArgument
     *
     * @param string $name 
     * @param string $default 
     * @return mixed
     */
    public function getArgument($name, $default = null)
    {
        if (isset($this->arguments[$name]) && $this->arguments[$name] !== null) {
            return $this->arguments[$name];
        } else {
            return $default;
        }
    }

    /**
     * getArguments
     *
     * @return array $arguments
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * setArguments
     *
     * @param array $args 
     * @return void
     */
    public function setArguments(array $args)
    {
        $this->arguments = $args;
    }

    /**
     * getTaskName
     *
     * @return string $taskName
     */
    public function getTaskName()
    {
        return $this->taskName;
    }

    /**
     * getDescription
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * getRequiredArguments
     *
     * @return array $requiredArguments
     */
    public function getRequiredArguments()
    {
        return array_keys($this->requiredArguments);
    }

    /**
     * getOptionalArguments
     *
     * @return array $optionalArguments
     */
    public function getOptionalArguments()
    {
        return array_keys($this->optionalArguments);
    }

    /**
     * getRequiredArgumentsDescriptions
     *
     * @return array $requiredArgumentsDescriptions
     */
    public function getRequiredArgumentsDescriptions()
    {
        return $this->requiredArguments;
    }

    /**
     * getOptionalArgumentsDescriptions
     *
     * @return array $optionalArgumentsDescriptions
     */
    public function getOptionalArgumentsDescriptions()
    {
        return $this->optionalArguments;
    }
}