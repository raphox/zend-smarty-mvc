                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ndex

                unset($e[3]); // Remove unused index
            }

            if (substr(trim($e[2]), 0, 1) != '(') {
                $expr = new Doctrine_Expression($e[2], $this->query->getConnection());
                $e[2] = $expr->getSql();
            }

            // We need to check for agg functions here
            $hasLeftAggExpression = preg_match('/(.*)\(([^\)]*)\)([\)]*)/', $e[0], $leftMatches);

            if ($hasLeftAggExpression) {
                $e[0] = $leftMatches[2];
            }

            $hasRightAggExpression = preg_match('/(.*)\(([^\)]*)\)([\)]*)/', $e[2], $rightMatches);

            if ($hasRightAggExpression) {
                $e[2] = $rightMatches[2];
            }

            $a         = explode('.', $e[0]);
            $field     = array_pop($a);
            $reference = implode('.', $a);
            $value     = $e[2];

            $conn      = $this->query->getConnection();
            $alias     = $this->query->getTableAlias($reference);
            $map       = $this->query->getAliasDeclaration($reference);
            $table     = $map['table'];

            // FIX: Issues with "(" XXX ")"
            if ($hasRightAggExpression) {
                $value = '(' . $value . ')';
            }

            if (substr($value, 0, 1) == '(') {
                // trim brackets
                $trimmed   = $this->_tokenizer->bracketTrim($value);

                if (substr($trimmed, 0, 4) == 'FROM' || substr($trimmed, 0, 6) == 'SELECT') {
                    // subquery found
                    $q = $this->query->createSubquery();

                    // Change due to bug "(" XXX ")"
                    //$value = '(' . $q->parseQuery($trimmed)->getQuery() . ')';
                    $value = $q->parseQuery($trimmed)->getQuery();
                } elseif (substr($trimmed, 0, 4) == 'SQL:') {
                    // Change due to bug "(" XXX ")"
                    //$value = '(' . substr($trimmed, 4) . ')';
                    $value = substr($trimmed, 4);
                } else {
                    // simple in expression found
                    $e     = $this->_tokenizer->sqlExplode($trimmed, ',');

                    $value = array();
                    foreach ($e as $part) {
                        $value[] = $this->parseLiteralValue($part);
                    }

                    // Change due to bug "(" XXX ")"
                    //$value = '(' . implode(', ', $value) . ')';
                    $value = implode(', ', $value);
                }
            } else {
                $value = $this->parseLiteralValue($value);
            }

            switch ($operator) {
                case '<':
                case '>':
                case '=':
                case '!=':
                default:
                    $leftExpr = (($hasLeftAggExpression) ? $leftMatches[1] . '(' : '') 
                              . $conn->quoteIdentifier($alias . '.' . $field)
                              . (($hasLeftAggExpression) ? $leftMatches[3] . ')' : '') ;

                    $rightExpr = (($hasRightAggExpression) ? $rightMatches[1] . '(' : '') 
                              . $value
                              . (($hasRightAggExpression) ? $rightMatches[3] . ')' : '') ;

                    $condition  = $leftExpr . ' ' . $operator . ' ' . $rightExpr;
            }

            return $condition;
        }
        
        $parser = new Doctrine_Query_Where($this->query, $this->_tokenizer);

        return $parser->parse($condition);
    }
}
