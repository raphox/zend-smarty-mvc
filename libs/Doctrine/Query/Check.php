                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                tion()
                        ->getTable($table);
        }
        $this->table = $table;
        $this->_tokenizer = new Doctrine_Query_Tokenizer();
    }

    /**
     * getTable
     * returns the table object associated with this object
     *
     * @return Doctrine_Connection
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * parse
     *
     * @param string $dql       DQL CHECK constraint definition
     * @return string
     */
    public function parse($dql)
    {
        $this->sql = $this->parseClause($dql);
    }

    /**
     * parseClause
     *
     * @param string $alias     component alias
     * @param string $field     the field name
     * @param mixed $value      the value of the field
     * @return void
     */
    public function parseClause($dql)
    {
        $parts = $this->_tokenizer->sqlExplode($dql, ' AND ');

        if (count($parts) > 1) {
            $ret = array();
            foreach ($parts as $part) {
                $ret[] = $this->parseSingle($part);
            }

            $r = implode(' AND ', $ret);
        } else {
            $parts = $this->_tokenizer->quoteExplode($dql, ' OR ');
            if (count($parts) > 1) {
                $ret = array();
                foreach ($parts as $part) {
                    $ret[] = $this->parseClause($part);
                }

                $r = implode(' OR ', $ret);
            } else {
                $ret = $this->parseSingle($dql);
                return $ret;
            }
        }
        return '(' . $r . ')';
    }
    
    public function parseSingle($part)
    {
        $e = explode(' ', $part);
        
        $e[0] = $this->parseFunction($e[0]);

        switch ($e[1]) {
            case '>':
            case '<':
            case '=':
            case '!=':
            case '<>':

            break;
            default:
                throw new Doctrine_Query_Exception('Unknown operator ' . $e[1]);
        }

        return implode(' ', $e);
    }
    public function parseFunction($dql) 
    {
        if (($pos = strpos($dql, '(')) !== false) {
            $func  = substr($dql, 0, $pos);
            $value = substr($dql, ($pos + 1), -1);
            
            $expr  = $this->table->getConnection()->expression;

            if ( ! method_exists($expr, $func)) {
                throw new Doctrine_Query_Exception('Unknown function ' . $func);
            }
            
            $func  = $expr->$func($value);
        }
        return $func;
    }

    /**
     * getSql
     *
     * returns database specific sql CHECK constraint definition
     * parsed from the given dql CHECK definition
     *
     * @return string
     */
    public function getSql()
    {
        return $this->sql;
    }
}