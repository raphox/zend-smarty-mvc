��������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������������/'
                                        => Doctrine::ERR_SYNTAX,
                                    '/column reference .* is ambiguous/i'
                                        => Doctrine::ERR_SYNTAX,
                                    '/column .* (of relation .*)?does not exist/i'
                                        => Doctrine::ERR_NOSUCHFIELD,
                                    '/attribute .* not found|relation .* does not have attribute/i'
                                        => Doctrine::ERR_NOSUCHFIELD,
                                    '/column .* specified in USING clause does not exist in (left|right) table/i'
                                        => Doctrine::ERR_NOSUCHFIELD,
                                    '/(relation|sequence|table).*does not exist|class .* not found/i'
                                        => Doctrine::ERR_NOSUCHTABLE,
                                    '/index .* does not exist/'
                                        => Doctrine::ERR_NOT_FOUND,
                                    '/relation .* already exists/i'
                                        => Doctrine::ERR_ALREADY_EXISTS,
                                    '/(divide|division) by zero$/i'
                                        => Doctrine::ERR_DIVZERO,
                                    '/pg_atoi: error in .*: can\'t parse /i'
                                        => Doctrine::ERR_INVALID_NUMBER,
                                    '/invalid input syntax for( type)? (integer|numeric)/i'
                                        => Doctrine::ERR_INVALID_NUMBER,
                                    '/value .* is out of range for type \w*int/i'
                                        => Doctrine::ERR_INVALID_NUMBER,
                                    '/integer out of range/i'
                                        => Doctrine::ERR_INVALID_NUMBER,
                                    '/value too long for type character/i'
                                        => Doctrine::ERR_INVALID,
                                    '/permission denied/'
                                        => Doctrine::ERR_ACCESS_VIOLATION,
                                    '/violates [\w ]+ constraint/'
                                        => Doctrine::ERR_CONSTRAINT,
                                    '/referential integrity violation/'
                                        => Doctrine::ERR_CONSTRAINT,
                                    '/violates not-null constraint/'
                                        => Doctrine::ERR_CONSTRAINT_NOT_NULL,
                                    '/more expressions than target columns/i'
                                        => Doctrine::ERR_VALUE_COUNT_ON_ROW,
                                );

    /**
     * This method checks if native error code/message can be
     * converted into a portable code and then adds this
     * portable error code to $portableCode field
     *
     * the portable error code is added at the end of array
     *
     * @param array $errorInfo      error info array
     * @since 1.0
     * @see Doctrine::ERR_* constants
     * @see Doctrine_Connection::$portableCode
     * @return boolean              whether or not the error info processing was successfull
     *                              (the process is successfull if portable error code was found)
     */
    public function processErrorInfo(array $errorInfo)
    {
        foreach (self::$errorRegexps as $regexp => $code) {
            if (preg_match($regexp, $errorInfo[2])) {
                $this->portableCode = $code;
                return true;
            }
        }
        return false;
    }
}