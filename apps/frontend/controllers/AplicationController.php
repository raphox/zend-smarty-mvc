<?php

/**
 * Classe herdada por todos os controller da aplicação
 * Esse classe é responsável por administrar a inicialização dos controllers
 *
 * @author Raphael Almeida Araújo <raphox.araujo@gmail.com>
 * @package Smarty com MVC
 * @version 0.6
 * @license GNU Version 2, June 1991
 */

class AplicationController extends Controller
{
  function __construct($appName)
  {
    parent::__construct($appName);
  }
}

?>