<?php

/**
 * Classe herdada por todos os controller da aplicação
 * Esse classe é responsável por administrar a inicialização dos controllers
 *
 * @author Raphael Almeida Araújo <raphox.araujo@gmail.com>
 * @package Smarty com MVC
 * @version 0.6
 * @license GNU Version 2, June 1991
 */

require_once(DIR_CONTROLLERS . DIRECTORY_SEPARATOR . 'AplicationController.php');

class IndexController extends AplicationController
{
  function carregarDados()
  {
    $this->grupos = array('' => ' Indiferente');
    $grupos = Doctrine::getTable('Grupo')->findAll();

    foreach ($grupos as $grupo)
    {
      $this->grupos[(string) $grupo->id] = $grupo->nome;
    }
  }

  function indexExecute()
  {
    $this->carregarDados();
    $this->pesquisarExecute();

    $this->render();
  }

  function pesquisarExecute()
  {
    $busca = '%' . $_POST['email'] . '%';
    $query = Doctrine_Query::create()
      ->from('Usuario u')
      ->addWhere('u.email LIKE ?', $busca);

    if (isset($_POST['grupo_id']) && !empty($_POST['grupo_id']))
    {
      $query->leftJoin('u.Grupos g')
        ->addWhere('g.id = ?', $_POST['grupo_id']);
    }

    $this->usuarios = $query->execute();

    switch ($this->getTypeRequest()) {
      case 'js':
        return $this->render(array('action' => '_listagem'));
        break;
    }
  }

  function editarExecute()
  {
    $this->carregarDados();
    unset($this->grupos['']);

    if ($id = View::getRequestVar('id'))
    {
      $query = Doctrine_Query::create()
        ->from('Usuario u')
        ->where('u.id = ?', $id)
        ->leftJoin('u.Grupos g');
      $this->usuario = $query->fetchOne();
      
      $this->grupos_selecionados = array();
      foreach ($this->usuario->Grupos as $grupo)
      {
        $this->grupos_selecionados[] = $grupo->id;
      }
    }
    else
    {
      $this->usuario = new Usuario();
    }

    if ($_POST)
    {
      $this->usuario->merge($_POST['usuario']);
      
      if ($this->usuario->exists())
      {
        $this->usuario->unlink('Grupos');
      }

      $this->usuario->save();
      
      $this->usuario->link('Grupos', $_POST['usuario']['grupos']);

      $this->flash['mensagem'] = "== Usuário salvo com sucesso ==";

      $this->redirect('/index/editar?id=' . $this->usuario->id);
    }

    $this->render();
  }

  public function deletarExecute()
  {
    Doctrine_Query::create()
      ->delete()
      ->from('Usuario')
      ->addWhere('id = ?', $_GET['id'])
      ->execute();

    $this->pesquisarExecute();
  }
}

?>