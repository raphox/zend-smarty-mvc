<?php /* Smarty version 2.6.20, created on 2008-09-13 15:59:39
         compiled from index/editar.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<script type="text/javascript" src="/js/html_div.js"></script>
		<script type="text/javascript" src="/js/jquery.js"></script>
		
		<link rel="stylesheet" type="text/css" media="screen" href="/css/html_div.css" />
		
		<title>Bem vindo ao FreedomDay</title>
	</head>
	<body>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "_topo.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<h2>Editar</h2>
		<?php if ($this->_tpl_vars['flash_vars']['mensagem'] != ''): ?><div class="mensagem"><?php echo $this->_tpl_vars['flash_vars']['mensagem']; ?>
</div><?php endif; ?>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'index/_form.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	</body>
</html>