<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		
		<script type="text/javascript" src="/js/html_div.js"></script>
		<script type="text/javascript" src="/js/jquery.js"></script>
		
		<link rel="stylesheet" type="text/css" media="screen" href="/css/html_div.css" />
		
		<title>Bem vindo ao FreedomDay</title>
	</head>
	<body>
		{include file="_topo.tpl"}
		<h2>Editar</h2>
		{if $flash_vars.mensagem ne ''}<div class="mensagem">{$flash_vars.mensagem}</div>{/if}
		{include file='index/_form.tpl'}
	</body>
</html>