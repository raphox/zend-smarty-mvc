<script type="text/javascript">
{literal}
	function deletar(usuario_id)
	{
		$.ajax({
			type: 'GET',
			url: '/index/deletar',
			data: 'id=' + usuario_id,
			success: function(data) {
				$('#listagem').html(data);
			}
		});
	}
{/literal}
</script>

<table width="70%">
	<tr>
		<th>Email</th>
		<th>Ações</th>
	</tr>
{foreach key=cid item=usuario from=$usuarios}
    <tr>
		<td>{$usuario.email}</td>
		<td align="center">
			<a href="/index/editar?id={$usuario.id}">editar</a>
			<a href="javascript:;" onclick="deletar({$usuario.id})">remover</a>
		</td>
	</tr>
{foreachelse}
	<tr>
		<td colspan="3" align="center">Nenhum usuário encontrado</td>
    </tr>
{/foreach}
</table>