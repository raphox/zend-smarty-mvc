<script type="text/javascript">
{literal}
$(document).ready(function() {
	$().ajaxStart(function() {
		$('#listagem').slideToggle("slow");
	}).ajaxStop(function() {
		$('#listagem').slideToggle("slow");
	});
 
	$('#pesquisa').submit(function() {
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data) {
				$('#listagem').html(data);
			}
		})
		return false;
	});
})
{/literal}
</script>

<form id="pesquisa" name="pesquisa" action="/index/pesquisar">
	<h3>Pesquisa</h3>
	<p><label>Grupo:</label> {html_options name=grupo_id options=$grupos}</p>
	<p><label>Email:</label> <input type="text" name="email" /></p>
	<p>
		<input type="submit" value="Pesquisar" />
		<input type="button" value="Adicionar" onclick="window.open('/index/editar', '_top');" />
	</p>
</form>