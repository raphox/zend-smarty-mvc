CREATE TABLE usuario_grupo (id BIGINT AUTO_INCREMENT, usuario_id INT, grupo_id INT, INDEX usuario_id_idx (usuario_id), INDEX grupo_id_idx (grupo_id), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE grupos (id INT AUTO_INCREMENT, nome VARCHAR(255), PRIMARY KEY(id)) ENGINE = INNODB;
CREATE TABLE usuarios (id INT AUTO_INCREMENT, email VARCHAR(255), senha VARCHAR(255), PRIMARY KEY(id)) ENGINE = INNODB;
ALTER TABLE usuario_grupo ADD FOREIGN KEY (usuario_id) REFERENCES usuarios(id) ON DELETE CASCADE;
ALTER TABLE usuario_grupo ADD FOREIGN KEY (grupo_id) REFERENCES grupos(id) ON DELETE CASCADE;
